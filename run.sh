#!/bin/bash

# Activate Environment
source activate pytorch_p36
# source activate pytorch

E=350

X=1;
echo Y | python main.py -x $X -E $E | tee resnet152.txt
echo Y | python ensemble.py -x $X -E $E | tee resnet18x5.txt

X=2;
echo Y | python main.py -x $X -E $E | tee resnet50.txt
echo Y | python ensemble.py -x $X -E $E | tee resnet18x2.txt

exit




