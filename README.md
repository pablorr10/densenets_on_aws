# Train CIFAR10 with PyTorch

I'm playing with [PyTorch](http://pytorch.org/) on the CIFAR10 dataset.

## Accuracy
| Model             | Acc.        |
| ----------------- | ----------- |
| [DenseNet121](https://arxiv.org/abs/1608.06993)       | 95.04%      |
| Ensemble of Dense Nets       |     ¿?      |

## Learning rate adjustment
The `lr` during training:
- `0.1` for epoch `[0,150)`
- `0.01` for epoch `[150,250)`
- `0.001` for epoch `[250,350)`

Resume the training with `python main.py --resume --lr=0.01`
